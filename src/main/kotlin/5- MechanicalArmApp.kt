class MechanicalArm {
    var angle:Double = 0.0
    var height:Double = 0.0
    var turnOn:Boolean = false

    fun toggle(){
        this.turnOn = !this.turnOn
        printState()
    }

    fun updateAngle(angle: Int){
        if(this.turnOn) this.angle += angle
        if(this.angle < 0.0) this.angle = 0.0
        else if(this.angle > 360.0) this.angle = 360.0
        printState()
    }

    fun updateAltitude(height: Int){
        if(this.turnOn) this.height += height
        if(this.height < 0.0) this.height = 0.0
        else if(this.height > 30.0) this.height = 30.0
        printState()
    }

    fun printState(){
        println("MechanicalArm{angle=${this.angle},height=${this.height},turnOn=${this.turnOn}}")
    }
}

fun main(){
    val mechanicalArm = MechanicalArm()
    mechanicalArm.toggle()
    mechanicalArm.updateAltitude(3)
    mechanicalArm.updateAngle(180)
    mechanicalArm.updateAltitude(-3)
    mechanicalArm.updateAngle(-180)
    mechanicalArm.updateAltitude(3)
    mechanicalArm.toggle()
}
