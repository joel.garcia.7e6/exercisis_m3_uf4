val RESET = "\u001B[0m"
val BLACK = "\u001B[40m"
val RED = "\u001B[41m"
val GREEN = "\u001B[42m"
val YELLOW = "\u001B[43m"
val BLUE = "\u001B[44m"
val PURPLE = "\u001B[45m"
val CYAN = "\u001B[46m"
val WHITE = "\u001B[47m"

val colors = arrayOf(BLACK, WHITE, RED, YELLOW, BLUE, PURPLE, CYAN,GREEN)
class Lampada(val id:Int){
    var color = 1
    var intensitat = 1
    var offColor = 1
    var on = false
    fun turnOn(){
        if(this.offColor in colors.indices){
            this.color = this.offColor
            this.intensitat=1
        }
        this.on = true
        estado()
    }
    fun turnOff(){
        if(this.color > 0){
            this.offColor = this.color
            this.color=0
            this.intensitat=0
        }
        this.on = false
        estado()
    }
    fun changeColor(){
        if(this.on){
            if (this.color < colors.lastIndex) this.color++
            else if(this.color == colors.lastIndex) this.color=1
        }
        estado()
    }

    fun intensity(){
        if(this.on){
            if(this.intensitat==5) this.intensitat = 1
            else this.intensitat++
        }
        estado()
    }
    fun estado(){
        println("Lampada: ${this.id} | Color: ${colors[this.color]}    $RESET - Intensitat ${this.intensitat}")
    }
}
fun main(){
    val lampada1 = Lampada(1)
    val lampada2 = Lampada(2)

    lampada1.turnOn()
    for(i in 1..3) lampada1.changeColor()
    for(i in 1..4) lampada1.intensity()

    lampada2.turnOn()
    for(i in 1..2) lampada2.changeColor()
    for(i in 1..4) lampada2.intensity()
    lampada2.turnOff()
    lampada2.changeColor()
    lampada2.turnOn()
    lampada2.changeColor()
    for(i in 1..4) lampada2.intensity()
}

