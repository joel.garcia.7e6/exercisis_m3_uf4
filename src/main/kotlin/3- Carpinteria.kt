import java.util.Scanner

fun main(){
    val sc = Scanner(System.`in`)
    val objecte = sc.nextLine().toInt()
    var preuTotal = 0.0
    for(i in 1..objecte){
        val separacioObjecte = sc.nextLine().split(' ')
        if(separacioObjecte[0]=="Taulell"){
            val taulell = Taulell(separacioObjecte[1].toDouble(),separacioObjecte[2].toDouble(),separacioObjecte[3].toDouble())
            preuTotal+=taulell.preuUnitat
        }else{
            val llisto = Llisto(separacioObjecte[1].toDouble(),separacioObjecte[2].toDouble())
            preuTotal+=llisto.preuUnitat
        }
    }
    println("El preu total és: $preuTotal€")
}

class Taulell(var preuUnitat: Double,val llargada: Double,val amplada:Double){
    init {
        val mida = this.llargada*this.amplada
        this.preuUnitat*=mida
    }
}

class Llisto(var preuUnitat:Double, val llargada:Double){
    init {
        this.preuUnitat *= this.llargada
    }
}
