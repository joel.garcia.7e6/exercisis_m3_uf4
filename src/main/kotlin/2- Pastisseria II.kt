

class Beguda(val nom:String, var preu:Double, val ensucarada:Boolean){
    fun showDrinkDetails(){
        if(ensucarada==true)preu+=preu*0.1
        println("$nom,$preu€,$ensucarada")
    }
}

val aigua=Beguda("aigua", 0.75, false)
val cafe=Beguda("café tallat", 1.0, false)
val te=Beguda("té vermell", 1.5, false)
val cola=Beguda("cola", 2.2, true)

fun main(){
    croissant.showSweetDetails()
    ensaimada.showSweetDetails()
    donut.showSweetDetails()
    aigua.showDrinkDetails()
    cafe.showDrinkDetails()
    te.showDrinkDetails()
    cola.showDrinkDetails()

}