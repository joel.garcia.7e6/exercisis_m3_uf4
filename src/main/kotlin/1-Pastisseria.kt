

class Pasta(val nom:String, val preu:Double, val pes:Int, val calories:Int){
    fun showSweetDetails() {
        println("$nom,$preu€,$pes g,$calories kcal")
    }
}
val croissant=Pasta("croissant", 1.2, 100, 406)
val ensaimada=Pasta("ensaimada", 3.6, 39, 166)
val donut=Pasta("donut", 0.9, 48, 199)

fun main() {
    croissant.showSweetDetails()
    ensaimada.showSweetDetails()
    donut.showSweetDetails()

}