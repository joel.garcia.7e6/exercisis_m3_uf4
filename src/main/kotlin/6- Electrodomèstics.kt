open class Electrodomestic (var preuBase:Int, var color:String = "blanc", var consum:Char = 'G', var pes:Int = 5){
    open fun preuFinal():Int{
        when(this.consum){
            'A' -> this.preuBase+=35
            'B' -> this.preuBase+=30
            'C' -> this.preuBase+=25
            'D' -> this.preuBase+=20
            'E' -> this.preuBase+=15
            'F' -> this.preuBase+=10
        }
        when(this.pes){
            in 6..20 -> this.preuBase+=20
            in 21..50 -> this.preuBase+=50
            in 51..80 -> this.preuBase+=80
        }
        if(this.pes > 80) this.preuBase+=100
        return this.preuBase
    }

    open fun imprimirAtributs(){
        println("     Preu base: ${this.preuBase}€")
        println("     Color: ${this.color}")
        println("     Consum: ${this.consum}")
        println("     Pes: ${this.pes}")
        println("     Preu final: ${this.preuFinal()}€")
    }

    open fun imprimirPreus(){
        println("Electrodomestic:")
        println("     Preu base: ${this.preuBase}€")
        println("     Preu final: ${this.preuFinal()}€")
    }

}

class Rentadora(preuBase: Int,var carrega: Int = 5):Electrodomestic(preuBase){
    override fun preuFinal():Int{
        when(this.carrega){
            in 6..7 -> this.preuBase+=55
            8 -> this.preuBase+=70
            9 -> this.preuBase+=85
            10 -> this.preuBase+=100
        }
        return this.preuBase
    }

    override fun imprimirAtributs() {
        super.imprimirAtributs()
        println("     Carrega: ${this.carrega}")
    }

    override fun imprimirPreus(){
        println("Rentadora:")
        println("     Preu base: ${this.preuBase}€")
        println("     Preu final: ${this.preuFinal()}€")
    }
}

class Televisio(preuBase: Int,var tamany: Int = 28):Electrodomestic(preuBase){
    override fun preuFinal():Int{
        when(this.tamany){
            in 29..32 -> this.preuBase+=50
            in 32..42 -> this.preuBase+=100
            in 43..50 -> this.preuBase+=150
        }
        if(this.tamany > 51) this.preuBase+=200
        return this.preuBase
    }

    override fun imprimirAtributs() {
        super.imprimirAtributs()
        println("     Tamany: ${this.tamany}")
    }

    override fun imprimirPreus(){
        println("Televisio:")
        println("     Preu base: ${this.preuBase}€")
        println("     Preu final: ${this.preuFinal()}€")
    }
}

fun main(){
    val electrodomestics = listOf(
        Electrodomestic(500,"blanc",'G'),
        Electrodomestic(400,"blanc",'A',70),
        Electrodomestic(200,"platejat",'D',50),
        Electrodomestic(100,"platejat"),
        Electrodomestic(100),
        Electrodomestic(100,"color"),
        Rentadora(550,5),
        Rentadora(700,8),
        Televisio(450,52),
        Televisio(200,28)
    )

    for (i in electrodomestics.indices){
        println("Electrodomestic ${i+1}:")
        electrodomestics[i].imprimirAtributs()
    }
    println()
    for (i in electrodomestics.indices){
        electrodomestics[i].imprimirPreus()
    }
}
